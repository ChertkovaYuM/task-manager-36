package ru.tsc.chertkova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.tsc.chertkova.tm.api.endpoint.*;

public interface IServiceLocator {

    @NotNull
    ICommandService getCommandService();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    ILoggerService getLoggerService();

    @NotNull
    ITaskEndpoint getTaskEndpoint();

    @NotNull
    IProjectEndpoint getProjectEndpoint();

    @NotNull
    ISystemEndpoint getSystemEndpoint();

    @NotNull
    IAuthEndpoint getAuthEndpoint();

    @NotNull
    IDomainEndpoint getDomainEndpoint();

    @NotNull
    IUserEndpoint getUserEndpoint();

    @NotNull
    ITokenService getTokenService();

}
