package ru.tsc.chertkova.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.tsc.chertkova.tm.dto.request.system.ApplicationAboutRequest;
import ru.tsc.chertkova.tm.dto.response.system.ApplicationAboutResponse;

public final class AboutCommand extends AbstractSystemCommand {

    @NotNull
    public static final String NAME = "about";

    @NotNull
    public static final String DESCRIPTION = "Display developer info.";

    @NotNull
    public static final String ARGUMENT = "-a";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        ApplicationAboutResponse response = serviceLocator.getSystemEndpoint()
                .getAbout(new ApplicationAboutRequest());
        System.out.println("Name: " + response.getName());
        System.out.println("E-mail: " + response.getEmail());
    }

}
